#initial time: 0.234 sec 
#after index: 0.062 sec
#after PK: 0.032 sec
use insertion;

select * from proteins;

load data local infile  'C:\\Users\\Muzo\\Desktop\\insert.txt' into table proteins fields terminated by '|';
select * from proteins;
set global innodb_purge_batch_size=5000;

select *
from proteins 
where protein_name like "%tumor%" and uniprot_id like "%human%"
order by uniprot_id;

create index uniprot_index on proteins(uniprot_id);
drop index uniprot_index on proteins;
alter table proteins add constraint pk_proteins primary key(uniprot_id);
alter table proteins drop primary key;